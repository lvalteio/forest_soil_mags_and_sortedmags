Supporting Documentation for "Complementary metagenomic approaches reveal bacterial and archaeal diversity in forest soil"

Alteio LV, Schulz F, Seshadri R, Varghese N, Rodriguez-Reillo W, Ryan EM, Goudeau D, Eichorst SA, Malmstrom RR, Katz LA, Blanchard J, Woyke T

This repository contains all phylogenetic trees and underlying sequence alignments described in the paper. Sorted-MAGs and MAGs of medium quality or higher are provided in this repository and will soon be available at GenBank.

Please contact Lauren Alteio (lalteio@umass.edu) for any questions regarding the data and their use.